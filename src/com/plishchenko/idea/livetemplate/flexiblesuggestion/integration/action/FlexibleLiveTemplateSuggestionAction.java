package com.plishchenko.idea.livetemplate.flexiblesuggestion.integration.action;

import com.intellij.codeInsight.CodeInsightActionHandler;
import com.intellij.codeInsight.actions.BaseCodeInsightAction;
import com.intellij.openapi.project.DumbAware;
import com.plishchenko.idea.livetemplate.flexiblesuggestion.integration.TemplatesSuggestionActionHandler;
import org.jetbrains.annotations.NotNull;

public class FlexibleLiveTemplateSuggestionAction extends BaseCodeInsightAction implements DumbAware {

  @NotNull
  @Override
  protected CodeInsightActionHandler getHandler() {
    return new TemplatesSuggestionActionHandler();
  }

  @Override
  protected boolean isValidForLookup() {
    return true;
  }
}