package com.plishchenko.idea.livetemplate.flexiblesuggestion.integration;

import static com.intellij.codeInsight.template.impl.TemplateListPanel.TEMPLATE_COMPARATOR;
import static com.plishchenko.idea.livetemplate.flexiblesuggestion.logic.TokensMatcher.isNameMatchQuery;

import com.intellij.codeInsight.CodeInsightActionHandler;
import com.intellij.codeInsight.lookup.Lookup;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupEvent;
import com.intellij.codeInsight.lookup.LookupListener;
import com.intellij.codeInsight.lookup.LookupManager;
import com.intellij.codeInsight.lookup.impl.LookupImpl;
import com.intellij.codeInsight.template.CustomLiveTemplate;
import com.intellij.codeInsight.template.CustomLiveTemplateBase;
import com.intellij.codeInsight.template.CustomTemplateCallback;
import com.intellij.codeInsight.template.TemplateManager;
import com.intellij.codeInsight.template.impl.CustomLiveTemplateLookupElement;
import com.intellij.codeInsight.template.impl.LiveTemplateLookupElement;
import com.intellij.codeInsight.template.impl.LiveTemplateLookupElementImpl;
import com.intellij.codeInsight.template.impl.TemplateImpl;
import com.intellij.codeInsight.template.impl.TemplateManagerImpl;
import com.intellij.featureStatistics.FeatureUsageTracker;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.ex.util.EditorUtil;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiFile;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.containers.MultiMap;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TemplatesSuggestionActionHandler implements CodeInsightActionHandler {

  @Override
  public void invoke(@NotNull final Project project, @NotNull final Editor editor, @NotNull PsiFile file) {
    EditorUtil.fillVirtualSpaceUntilCaret(editor);

    PsiDocumentManager.getInstance(project).commitDocument(editor.getDocument());
    int offset = editor.getCaretModel().getOffset();
    List<TemplateImpl> applicableTemplates = TemplateManagerImpl.listApplicableTemplateWithInsertingDummyIdentifier(editor, file, false);

    Map<TemplateImpl, String> matchingTemplates = filterTemplatesByPrefix(applicableTemplates, editor, offset);
    MultiMap<String, CustomLiveTemplateLookupElement> customTemplatesLookupElements = getCustomTemplatesLookupItems(editor, file, offset);

    if (matchingTemplates.isEmpty() && customTemplatesLookupElements.isEmpty()) {
      return;
    }

    showTemplatesLookup(project, editor, file, matchingTemplates, customTemplatesLookupElements);
  }

  public static Map<TemplateImpl, String> filterTemplatesByPrefix(@NotNull Collection<? extends TemplateImpl> templates, @NotNull Editor editor, int offset) {
    String prefixWithoutDots = computeDescriptionMatchingPrefix(editor.getDocument(), offset);

    // ?? preliminary filtration
    Map<TemplateImpl, String> matchingTemplates = new TreeMap<>(TEMPLATE_COMPARATOR);
    templates.forEach(template -> {
      ProgressManager.checkCanceled();
      if (isNameMatchQuery(template.getKey(), prefixWithoutDots)) {
        matchingTemplates.put(template, prefixWithoutDots);
      }
    });

    return matchingTemplates;
  }

  private static void showTemplatesLookup(Project project, Editor editor, PsiFile file,
      Map<TemplateImpl, String> matchingTemplates,
      MultiMap<String, CustomLiveTemplateLookupElement> customTemplatesLookupElements) {

    LookupImpl lookup = (LookupImpl)LookupManager.getInstance(project).createLookup(editor, LookupElement.EMPTY_ARRAY, "", new FlTemplatesArranger());// set arranger here
    matchingTemplates.forEach((template, value) -> {
      lookup.addItem(createTemplateElement(template), new FlPrefixMatcher(StringUtil.notNullize(value)));// set matcher here
    });

    customTemplatesLookupElements.entrySet().forEach(entry -> {
      entry.getValue().forEach(lookupElement ->
          lookup.addItem(lookupElement, new FlPrefixMatcher(entry.getKey())));//?? set matcher here
    });

    showLookup(lookup, file);
  }

  public static MultiMap<String, CustomLiveTemplateLookupElement> getCustomTemplatesLookupItems(@NotNull Editor editor,
      @NotNull PsiFile file,
      int offset) {
    final MultiMap<String, CustomLiveTemplateLookupElement> result = MultiMap.create();
    CustomTemplateCallback customTemplateCallback = new CustomTemplateCallback(editor, file);
    for (CustomLiveTemplate customLiveTemplate : TemplateManagerImpl.listApplicableCustomTemplates(editor, file, false)) {
      if (customLiveTemplate instanceof CustomLiveTemplateBase) {
        String customTemplatePrefix = ((CustomLiveTemplateBase)customLiveTemplate).computeTemplateKeyWithoutContextChecking(customTemplateCallback);
        if (customTemplatePrefix != null) {
          result.putValues(customTemplatePrefix, ((CustomLiveTemplateBase)customLiveTemplate).getLookupElements(file, editor, offset));
        }
      }
    }
    return result;
  }

  private static LiveTemplateLookupElement createTemplateElement(final TemplateImpl template) {
    return new LiveTemplateLookupElementImpl(template, false) {
      @Override
      public Set<String> getAllLookupStrings() {
        String description = template.getDescription();
        if (description == null) {
          return super.getAllLookupStrings();
        }
        return ContainerUtil.newHashSet(getLookupString(), description);
      }
    };
  }

  private static void showLookup(LookupImpl lookup, @NotNull PsiFile file) {
    lookup.addLookupListener(new MyLookupAdapter(file));
    lookup.refreshUi(false, true);
    lookup.showLookup();
  }

  private static String computeDescriptionMatchingPrefix(Document document, int offset) {
    CharSequence chars = document.getCharsSequence();
    int start = offset;
    while (true) {
      ProgressManager.checkCanceled();
      if (start == 0) break;
      char c = chars.charAt(start - 1);
      if (!(Character.isJavaIdentifierPart(c))) break;
      start--;
    }
    return chars.subSequence(start, offset).toString();
  }

  private static class MyLookupAdapter implements LookupListener {
    private final Map<TemplateImpl, String> myTemplate2Argument;
    private final PsiFile myFile;

    MyLookupAdapter(@Nullable PsiFile file) {
      myTemplate2Argument = null;
      myFile = file;
    }

    @Override
    public void itemSelected(@NotNull final LookupEvent event) {
      FeatureUsageTracker.getInstance().triggerFeatureUsed("codeassists.liveTemplates");
      final LookupElement item = event.getItem();
      final Lookup lookup = event.getLookup();
      final Project project = lookup.getProject();
      if (item instanceof LiveTemplateLookupElementImpl) {
        final TemplateImpl template = ((LiveTemplateLookupElementImpl)item).getTemplate();
        final String argument = myTemplate2Argument != null ? myTemplate2Argument.get(template) : null;
        WriteCommandAction.writeCommandAction(project).run(() -> ((TemplateManagerImpl)TemplateManager.getInstance(project)).startTemplateWithPrefix(lookup.getEditor(), template, null, argument));
      }
      else if (item instanceof CustomLiveTemplateLookupElement) {
        if (myFile != null) {
          WriteCommandAction.writeCommandAction(project).run(() -> ((CustomLiveTemplateLookupElement)item).expandTemplate(lookup.getEditor(), myFile));
        }
      }
    }
  }

}