package com.plishchenko.idea.livetemplate.flexiblesuggestion.integration;

import com.intellij.codeInsight.completion.PrefixMatcher;
import com.intellij.codeInsight.lookup.LookupElement;
import com.plishchenko.idea.livetemplate.flexiblesuggestion.logic.TokensMatcher;
import org.jetbrains.annotations.NotNull;


public class FlPrefixMatcher extends PrefixMatcher {

  FlPrefixMatcher(String prefix) {
    super(prefix);
  }

  @Override
  @NotNull
  public PrefixMatcher cloneWithPrefix(@NotNull String prefix) {
    return new FlPrefixMatcher(prefix);
  }

  @Override
  public boolean isStartMatch(String name) {
    return TokensMatcher.isNameMatchQuery(name, myPrefix);
  }

  @Override
  public boolean isStartMatch(LookupElement element) {
    return TokensMatcher.isNameMatchQuery(element.getLookupString(), myPrefix);
  }

  @Override
  public boolean prefixMatches(@NotNull final String name) {
    return TokensMatcher.isNameMatchQuery(name, myPrefix);
  }

  @Override
  public boolean prefixMatches(@NotNull final LookupElement element) {
    return TokensMatcher.isNameMatchQuery(element.getLookupString(), myPrefix);
  }

  @Override
  public String toString() {
    return myPrefix;
  }

  @Override
  public int matchingDegree(String string) {
     return 0;
  }
}