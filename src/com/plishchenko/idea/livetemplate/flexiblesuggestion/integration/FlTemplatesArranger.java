package com.plishchenko.idea.livetemplate.flexiblesuggestion.integration;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Comparator.comparingInt;

import com.intellij.codeInsight.lookup.Lookup;
import com.intellij.codeInsight.lookup.LookupArranger;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.openapi.util.Pair;
import com.plishchenko.idea.livetemplate.flexiblesuggestion.logic.TokensMatcher;
import java.util.List;
import org.jetbrains.annotations.NotNull;

class FlTemplatesArranger extends LookupArranger {

  @Override
  public Pair<List<LookupElement>, Integer> arrangeItems(@NotNull Lookup lookup, boolean onExplicitAction) {

    // sort
    List<LookupElement> sortedElements = newArrayList(getMatchingItems());
    sortedElements.sort(comparingInt((LookupElement lElement) -> calculateMatchingQuality(lookup, lElement)).reversed());

    // return sorted + selection info
    int selected = lookup.isSelectionTouched() ? sortedElements.indexOf(lookup.getCurrentItem()) : 0;
    return new Pair<>(sortedElements, Math.max(selected, 0));
  }

  private int calculateMatchingQuality(Lookup lookup, LookupElement lElement) {
    String name = lElement.getLookupString();
    String query = lookup.itemPattern(lElement);
    return TokensMatcher.calculateMatchingQuality(name, query);
  }

  @Override
  public LookupArranger createEmptyCopy() {
    return new FlTemplatesArranger();
  }
}
