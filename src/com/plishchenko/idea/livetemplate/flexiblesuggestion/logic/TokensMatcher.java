package com.plishchenko.idea.livetemplate.flexiblesuggestion.logic;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang.StringUtils.getCommonPrefix;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class TokensMatcher {

  private static final int MAX_HASH_VALUE = 1000;
  private static final int MAX_ADVICE_VALUE = 10;
  private static final int MAX_COMMON_PREFIX_VALUE = 100;
  private static final int MAX_NAME_LENGTH = 100;

  private static final String SPLIT_BY_CAPITAL_PATTERN_STR = "(?=\\p{Lu})";
  private static final String SPLIT_BY_NUMBER_PATTERN_STR = "(?<=\\p{L})(?=\\p{N})|(?<=\\p{N})(?=\\p{L})";
  private static final String NAME_SPLIT_PATTERN_STR = SPLIT_BY_CAPITAL_PATTERN_STR + "|" + SPLIT_BY_NUMBER_PATTERN_STR;
  private static final String ORDERING_ADVICE_PATTERN_STR = "\\\\([0-9]+)";

  private static final Pattern NAME_SPLIT_PATTERN = Pattern.compile(NAME_SPLIT_PATTERN_STR);
  private static final Pattern EXTRACT_WORDS_PATTERN = Pattern.compile("[a-zA-Z0-9]+");
  private static final Pattern ORDERING_ADVICE_PATTERN = Pattern.compile(ORDERING_ADVICE_PATTERN_STR);
  private static final Pattern SEARCH_TAG_PATTERN = Pattern.compile("\\|");//tokens used just for search, not used for ordering


  public static boolean isNameMatchQuery(String name, String query) {
    if (query.isEmpty()) {
      return true;
    }

    Set<String> nameTerms = extractTermsFromString(name);
    List<Set<String>> queryTermsCombinations = calculateStringCombinations(query.toLowerCase(), nameTerms.size());

    return queryTermsCombinations.stream()
        .anyMatch(queryTerms -> isNameMatchQuery(nameTerms, queryTerms));
  }

  private static boolean isNameMatchQuery(Set<String> nameTerms, Set<String> queryTerms) {
    return queryTerms.stream()
        .allMatch(queryTerm -> nameTerms.stream()
            .anyMatch(nameTerm -> nameTerm.startsWith(queryTerm)));
  }

  private static List<Set<String>> calculateStringCombinations(String source, int maxPartsCnt) {
    if (source.isEmpty()) {
      return newArrayList(newHashSet());
    }

    if (maxPartsCnt == 1) {
      List<Set<String>> res = newArrayList();
      res.add(newHashSet(source));
      return res;
    }

    List<Set<String>> result = newArrayList();
    for (int firstPartLength = 1; firstPartLength <= source.length(); firstPartLength++) {
      String firstPart = source.substring(0, firstPartLength);
      List<Set<String>> restPartsCombinations = calculateStringCombinations(source.substring(firstPartLength), maxPartsCnt - 1);
      restPartsCombinations.forEach(rComb -> rComb.add(firstPart));
      if (restPartsCombinations.isEmpty()) {
        restPartsCombinations.add(newHashSet(firstPart));
      }
      result.addAll(restPartsCombinations);
    }
    return result;
  }

  private static Stream<String> splitByCapitalAndNumber(String query) {
    return stream(NAME_SPLIT_PATTERN.split(query))
        .map(String::toLowerCase);
  }

  private static Set<String> extractTermsFromString(String name) {
    Matcher wordMatcher = EXTRACT_WORDS_PATTERN.matcher(name);

    Set<String> words = new HashSet<>();
    while (wordMatcher.find()) {
      words.add(wordMatcher.group());
    }

    return words.stream()
        .flatMap(TokensMatcher::splitByCapitalAndNumber)
        .map(String::toLowerCase)
        .collect(toSet());
  }









  public static int calculateMatchingQuality(String name, String query) {

    int byCommonPrefix = calcMatchingQualityByCommonPrefix(name, query);
    int lengthBeforeAdvice = calcMeaningfulLength(name);
    int adviceValue = extractOrderingAdviceValue(name);
    int hashValue = calcNumericHash(name.substring(0, lengthBeforeAdvice), MAX_HASH_VALUE);

    return (((
        min(MAX_COMMON_PREFIX_VALUE, byCommonPrefix))
        * MAX_NAME_LENGTH + MAX_NAME_LENGTH - min(MAX_NAME_LENGTH, lengthBeforeAdvice))
        * MAX_ADVICE_VALUE + MAX_ADVICE_VALUE - min(MAX_ADVICE_VALUE, adviceValue))
        * MAX_HASH_VALUE + min(MAX_HASH_VALUE, hashValue) ;
  }

  private static int calcNumericHash(String name, int maxHashValue) {
    int sum = 0;
    char[] charArray = name.toCharArray();
    for (int i = 0; i < charArray.length; i++) {
      sum += charArray[i]*i;
    }

    return sum / maxHashValue;
  }

  private static int extractOrderingAdviceValue(String name) {
    Matcher adviceMatcher = ORDERING_ADVICE_PATTERN.matcher(name);

    Integer advice = adviceMatcher.find()
        ? Integer.parseInt(adviceMatcher.group().substring(1))
        : null;

    if (advice == null) {
      return 2;
    } else if (advice == 1) {
      return 1;
    } else {
      return 1 + advice;
    }
  }

  private static int calcMeaningfulLength(String name) {
    Matcher orderingAdviceMatcher = ORDERING_ADVICE_PATTERN.matcher(name);
    Matcher searchTagMatcher = SEARCH_TAG_PATTERN.matcher(name);
    int lineTerminators = name.length();
    if (orderingAdviceMatcher.find()) {
      lineTerminators = min(max(orderingAdviceMatcher.start() - 1, 0), lineTerminators);
    }
    if (searchTagMatcher.find()) {
      lineTerminators = min(max(searchTagMatcher.start() - 1, 0), lineTerminators);
    }

    return lineTerminators;
  }

  private static int calcMatchingQualityByCommonPrefix(String name, String query) {
    return getCommonPrefix(new String[]{name.toLowerCase(), query.toLowerCase()}).length();
  }
}
